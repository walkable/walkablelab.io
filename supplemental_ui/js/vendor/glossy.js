function glossyLookUp (term) {
  return (typeof window.glossyDict !== 'undefined') && window.glossyDict[term]
}

function htmlToElement (html) {
  var template = document.createElement('template')
  html = html.trim() // Never return a text node of whitespace as the result
  template.innerHTML = html
  return template.content.firstChild
}

function setTerm (instance, term) {
  var definition = glossyLookUp(term) || 'Not found'
  var element = htmlToElement('<div><b>' + term + '</b></br>' +
                                    definition +
                                    '</div>')
  element.querySelectorAll('.glossy-term').forEach(function (childTerm) {
    childTerm.addEventListener('click', function () {
      setTerm(instance, childTerm.dataset.glossyTerm)
    })
  })
  instance._element = element
  instance.setContent(element)
}

window.tippy('.glossy-term', {
  interactive: true,
  allowHTML: true,
  content: 'Loading...',
  followCursor: 'horizontal',
  theme: 'light',
  onCreate: function (instance) {
    window.myTippy = instance
  },
  onHidden: function (instance) {
    instance._element = null
    instance.setContent('Loading...')
  },
  onShow: function (instance) {
    var term = instance.reference.dataset.glossyTerm
    setTerm(instance, term)
  },
})
