module.exports = {
    purge: {
	enabled: true,
	content: [
	'./public/index.html'
	],
    },
  darkMode: false, // or 'media' or 'class'
  variants: {
    extend: {},
  },
  theme: {
      extend: {
	  keyframes: {
	  	slide: {
	  	    '0%, 30%, 100%': {opacity: 1},
	  	    '33%, 97%': {opacity: 0},
	  	},
	  },
	  animation: {
	  	slide: 'slide 30s ease-in-out infinite',
	  },
	  gridTemplateColumns: {
	  	'sd': '128px minmax(0, 1fr)',
	  },
      },
  },
  plugins: [],
}
