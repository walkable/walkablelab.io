= Frequently Asked Questions

Answers to Questions and/or Strong Assertions :)

[qanda]
Walkable is too high-level? You don't love SQL?::

TLDR; version: Walkable is both high and low level. We embraces SQL
the way Honeysql/Hugsql does.
+
People may have this impression because of the use of EQL. Walkable
exposes EQL for clients, but also have an adaption layer (that are
hidden from clients) where you can use s-expressions (something like
honeysql) to build SQL constructs.

But SQL is declarative. If you are not writing SQL directly, you're doing it wrong!::

The term declarative are inherently relative. If you use Clojure to
solve problems that logic programming languages are superior at, your
programming task will be tedious. In a similar manner, SQL does not
fit well with building APIs which are often nested structures unlike
flat tables often seen in SQL.

Why not generate Walkable registry from database schema?::

Walkable offers many things that are simply not part of the database
schema. For instance, there is no obvious way to infer authorization
constraints from database schema. Also, you don't always want to
expose every single column of every table of the database, right?
+
Besides the concern of private data, there's another aspect: Walkable
operates on a high level manner, similar to the way some SQL views
abstract away the implementation details of the underlying tables. Can
you infer SQL views from a schema?  Absolutely no!
