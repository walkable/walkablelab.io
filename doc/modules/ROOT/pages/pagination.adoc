= Pagination

Walkable supports SQL pagination you’ve been familiar with: `+OFFSET+`,
`+LIMIT+` and `+ORDER BY+`.

== In query parameters

You can specify how to paginate your query by providing any combination
of pagination keywords, namely: `+:offset+`, `+:limit+`, `+:order-by+`.

* No pagination:

[tabs]
====
Query::
+
--

[source,clojure]
----
`[{(:farmers/list {})
   [:farmer/number :farmer/name]}]]
;; the same as:
`[{:farmers/list
   [:farmer/number :farmer/name]}]]
----
--
SQL output::
+
--
[source,sql]
----
SELECT farmer.number, farmer.name FROM farmer
----
--
====

* Pagination with `+:limit+` and `+:order-by+`:


[tabs]
====
Query::
+
--

[source,clojure]
----
`[{(:farmers/list {:limit 3 :order-by :farmer/number})
   [:farmer/number :farmer/name]}]]
----


--
SQL output::
+
--


[source,sql]
----
SELECT farmer.number, farmer.name
 FROM farmer
 LIMIT 3
 ORDER BY farmer.number
----

--
====

* Who says `+:order-by+` has to be simple?


[tabs]
====
Query::
+
--

[source,clojure]
----
`[{(:farmers/list {:order-by [:farmer/number :farmer/name :desc :nils-first]})
   [:farmer/number :farmer/name]}]]
----


--
SQL output::
+
--


[source,sql]
----
SELECT farmer.number, farmer.name
FROM farmer
ORDER BY farmer.number, farmer.name DESC NULLS FIRST
----

--
====

== Validator and default value

Pagination can be a bomb itself if you let client apps specify
whatever parameters. You can just tell Walkable how pagination
parameters must comply and default values when provided parameters
fail that (or not even provided at all).

Validators and default values can be declared for each root or join in
the registry.

=== Offset and limit

For offset and limit, value of default must be a number while
validator is a function that check if the supplied parameter satisfies
your constraint.


[tabs]
====
Registry::
+
--
[source,clojure]
----
{:key :farmers/list
 :type :root
 :table "farmer"
 :default-offset  2
 :validate-offset #(<= 0 % 100)}
----
--
Query::
+
--
[source,clojure]
----
`[{(:farmers/list {:offset 9999})
   [:farmer/number :farmer/name]}]]
----

--
SQL output::
+
--

Default value `+2+` will be used because the supplied one (`+9999+`)
fails the validator:

[source,sql]
----
SELECT farmer.number, farmer.name
FROM farmer
OFFSET 2
----

--
====


NOTE: Walkable will check if the supplied argument is an integer
first, so you don’t have to do it in your validator functions.

=== Order-by

For order-by, default value must be a valid order-by expression while
validator is a function that check if all the supplied *columns*
satisfies your constraint. Usually you use a simple *set* as your
validator.

[tabs]
====
Registry::
+
--
[source,clojure]
----
{:key :farmers/list
 :type :root
 :table "farmer"
 :default-order-by [:farmer/number :asc]
 :validate-order-by #{:farmer/number :farmer/yob}}
----
--
Query::
+
--
[source,clojure]
----
`[{(:farmers/list {:order-by [:farmer/number :desc :farmer/yob :nils-first]})
   [:farmer/number :farmer/name]}]]
----
--
SQL output::
+
--
Supplied columns in `+:order-by+` satisfy the validator.

[source,sql]
----
SELECT farmer.number, farmer.name
FROM farmer
ORDER BY farmer.number DESC, farmer.yob NULLS FIRST
----

--
====


[tabs]
====
Registry::
+
--

[source,clojure]
----
{:key :farmers/list
 :type :root
 :table "farmer"
 :default-order-by [:farmer/number :asc]
 :validate-order-by #{:farmer/number :farmer/yob}}
----
--
Query::
+
--
[source,clojure]
----
`[{(:farmers/list {:order-by [:farmer/number :desc :farmer/name :nils-first]})
   [:farmer/number :farmer/name]}]]
----
--
SQL output::
+
--
The column `+:farmer/name+` in `+:order-by+` fails the validator.
Default value will be used:

[source,sql]
----
SELECT farmer.number, farmer.name
FROM farmer
ORDER BY farmer.number ASC
----
--
====
