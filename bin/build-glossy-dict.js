const asciidoctor = require('asciidoctor.js')()
const fs = require('fs')
const registry = asciidoctor.Extensions.create()
require('../lib/glossary.js')(registry)

function btoa (string) {
  return Buffer.from(string).toString('base64')
}

var glossyDict = {}

function extractGlossary(partial) {
    const content = fs.readFileSync('./doc/modules/ROOT/partials/' + partial + '.adoc')
    const doc = asciidoctor.load(content, { 'extension_registry': registry })

    const dlist = doc.getBlocks()[0].getItems()

    dlist.forEach(function(row) {
        var term = row[0][0].getText()
        var definition = row[1].getText()
        glossyDict[term] = definition
    })
}

extractGlossary('walkable-glossary')
extractGlossary('pathom-glossary')

var encodedDict = btoa(JSON.stringify(glossyDict))

fs.writeFileSync('supplemental_ui/js/glossy-dict.js', `window.glossyDict = JSON.parse(atob('${encodedDict}'))`)
