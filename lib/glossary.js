/*

Add Glossary functionality

Usage:

glossary:NameWithoutSpaces[]
glossary:TermFoo[term=name with spaces]
glossary:Name,With,Commas[]
glossary:TermFoo[display-text=synonymOfTermFoo,term=name without commas]
glossary:TermFoo[display-text=nice name without commas]
*/

function inlineDefinition (attrs) {
  var displayText = attrs['display-text'] || attrs.term
  return `<span class="glossy-term" data-glossy-term="${attrs.term}">${displayText}</span>`
}

module.exports = function (registry) {
  registry.inlineMacro('glossary', function () {
    var self = this
    self.process(function (parent, term, attrs) {
      attrs.term = attrs.term || term
      var result = inlineDefinition(attrs)
      return self.createInline(parent, 'quoted', result)
    })
  })
}
