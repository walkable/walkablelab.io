(ns clj.home
  (:require [hiccup.core :as html]
            [clj.svg :refer [svg-names]]
            [clojure.string :as str]))

;; click-tabs
(defn click-tab-radio [tabset tab]
  (format "click-tab-radio-%s%s" tabset tab))

(defn click-tab-content [tabset tab]
  (format "click-tab-content-%s%s" tabset tab))

(defn click-tabs-css-selectors []
  (for [tabset (range 1 5)
        tab '[a b c d e f]
        :let [radio (click-tab-radio tabset tab)
              content (click-tab-content tabset tab)]]
    [(str "#" content)
     (format "#%s:checked ~ #%s" radio content)]))

(defn click-tabs-css []
  (let [selectors (click-tabs-css-selectors)
        items-to-hide (mapv first selectors)
        items-to-show (mapv second selectors)]
    (format "%s { display: none;}\n%s { display: block;}"
            (clojure.string/join "," items-to-hide)
            (clojure.string/join "," items-to-show))))

(defn click-tabset-radios [id n]
  (map (fn [i checked]
         (let [c (char (+ 97 i))]
           [:input.hidden
            {:id (click-tab-radio id c)
             :name (format "click-tabset-%s" id)
             :type "radio"
             :checked checked}]))
       (range n)
       (cons true (repeat false))))

(defn def-click-tabset-labels
  [id & labels]
  (map-indexed (fn [i [tag m & children]]
                 (let [c (char (+ 97 i))
                       id-string (click-tab-radio id c)]
                   (into [tag (assoc m :for id-string)]
                         children)))
               labels))

(defn def-click-tabset-contents
  [id & contents]
  (map-indexed (fn [i [tag m & children]]
                 (let [c (char (+ 97 i))
                       id-string (click-tab-content id c)]
                   (into [tag (assoc m :id id-string)]
                         children)))
               contents))

;; hover-tabs
(defn hover-tab-label [tab]
  (format "hover-tab-label-%s" tab))

(defn hover-tab-content [tab]
  (format "hover-tab-content-%s" tab))

(defn hover-tabs-css-selectors []
  (for [tab-1 '[a b c d e f]            ;; (char-range 6)
        tab-2 '[a b c d e f] 
        :let [label (hover-tab-label tab-1)
              content (hover-tab-content tab-2)]]
    [(format ".%s:hover ~ .%s" label content)
     (= tab-1 tab-2)]))

(defn hover-tabs-css []
  (let [selectors (hover-tabs-css-selectors)
        items-to-show (into [] (comp (filter #(second %)) (map first)) selectors)
        items-to-hide (into [] (comp (filter #(not (second %))) (map first)) selectors)]
    (format "%s { display: none}\n%s { display: block;}"
            (clojure.string/join ", " items-to-hide)
            (clojure.string/join ", " items-to-show))))

(defn with-class [c m]
  (if (string? (:class m))
    (update m :class #(str % " " c))
    (assoc m :class c)))

(comment
  (with-class "foo" {:class "bar"})
  {:class "bar foo"}
  
  (with-class "foo" {})
  {:class "foo"})

(defn char-range [& args]
  (map #(char (+ 97 %)) (apply range args)))

(defn def-hover-tabset-labels
  [& labels]
  (map (fn [c [tag m & children]]
         (let [class-string (hover-tab-label c)]
           (into [tag (with-class class-string m)]
                 children)))
       (char-range)
       labels))

(defn def-hover-tabset-contents
  [& contents]
  (map (fn [c [tag m & children]]
         (let [class-string (hover-tab-content c)]
           (into [tag (with-class class-string m)]
                 children)))
       (char-range)
       contents))

(defn svg
  [name & [element]]
  (into [(or element :svg)] (get svg-names name)))

(defn svg->defs*
  ([svg-name] (svg->defs* svg-name (name svg-name)))
  ([svg-name id]
   (let [[opts body] (get svg-names svg-name)]
     {:id id
      :defs [:svg.hidden opts [:defs {} [:g {:id id} body]]]
      :gen `(fn [& [element#]]
              [(or element# :svg)
               ~opts
               [:use {:xlink:href (str "#" ~id)}]])})))

(defmacro svg->defs
  ([& args]
   (let [{:keys [id defs gen]} (apply svg->defs* args)
         defname (symbol (str "svg:defs:" id))
         defgen (symbol (str "svg:" id))]
     `(do (def ~defname ~defs)
          (def ~defgen ~gen)))))

(svg->defs :single-layer)

(svg->defs :star)

(def saves-time-content
  [[:vitruvian-man
    "Built for human"
    "structure your app in an intention-preserving manner"
    "tolerance towards human errors"
    "making change is easy"]
   [:grumpy-cat
    "Grumpy-coder compliance"
    "Just Does Its Job™"
    "no nonsense: no ORM, objects or classes. Only data and occasionally functions"
    "won't get in your way when you know exactly what you want"]
   [:box
    "Convenience out of the box"
    "dynamic filtering and pagination for free"
    "security by default"
    "EQL flexibility, extensibility and tooling"]])

(defn saves-time-item
  [[icon title & items]]
  [:div.flex-1 {}
   (svg icon :svg.mx-auto.my-4.h-20.fill-current.text-gray-800)
   [:h2.ml-4.font-medium.text-gray-800 {} title]
   (into [:ul.list-disc.px-4.text-sm.leading-normal {}]
         (mapv #(do [:li %]) items))])

(defn saves-time-section []
  [:div.py-12.bg-blue-50 {}
   [:h1.mb-4.text-center.text-xl.text-gray-800 {}
    "Walkable saves you time"]
   (into [:div.px-8.py-4.flex.mx-auto.flex-col.space-y-4.md:space-y-0.md:flex-row.md:space-x-4 {}]
         (map saves-time-item saves-time-content))])
;; :div.flex.flex-col.justify-between.w-24.md:flex-row.md:w-48.md:items-end.md:py-1
(defn hero-section []
  [:div.text-center.space-y-6.px-4.py-2.pb-8.md:pt-8 {}
   [:div.group.flex.flex-row.justify-center {}
    (svg :logo :svg.h-20.w-20.my-2.mr-4)
    [:div.flex.flex-col.w-48.justify-center
     (svg :logo-title :svg.w-48.fill-current.text-gray-800.group-hover:text-gray-600)
     (svg :logo-tagline :svg.fill-current.text-gray-900.group-hover:text-gray-700)]]
   [:div {}
    [:p {} "Bring not only Clojure but also "
     [:a.text-green-500.glow {:href "https://www.youtube.com/watch?v=EDRRbuWuUbQ"} "the joy of it "]]
    [:p {} "to your SQL-powered world"]]
   [:a.group.flex.flex-col.md:flex-row.justify-center
    {:href "https://www.youtube.com/watch?v=EDRRbuWuUbQ"}
    (svg :lyrics :svg.h-40.md:order-last)
    (svg :lennon :svg.h-40)]
   [:div.flex.flex-col.md:flex-row.justify-center.space-x-0.md:space-x-4.space-y-4.md:space-y-0 {}
    [:a.py-2.px-4.rounded-sm.bg-green-400.text-white.border.border-green-500
     {:href "/walkable/1.3.0/introduction.html"}
     "Getting started"]
    [:a.py-2.px-4.rounded-sm.text-green-500.border.border-green-500
     {:href "https://github.com/walkable-server/walkable"}
     "View the Source"]]])

(defn convince-section []
  [:div.py-12 {}
   [:h1.text-center.text-xl.text-gray-800 {}
    "Still not convinced?"]
   [:p.my-4.text-center "try these"]
   [:div.flex.flex-col.md:flex-row.justify-center.space-x-0.md:space-x-4.space-y-4.md:space-y-0.px-4 {}
    [:a.py-2.px-4.md:py-1.md:px-2.rounded-sm.border.text-green-500.border-green-500
     {:href "https://github.com/walkable-server/realworld-fulcro"}
     "Realworld Fulcro"]
    #_[:a.py-2.px-4.md:py-1.md:px-2.rounded-sm.text-gray-500.border.border-gray-500
     {:href "https://github.com/walkable-server/minimalistic"}
     "Minimal Project"]
    #_[:a.py-2.px-4.md:py-1.md:px-2.rounded-sm.text-gray-500.border.border-gray-500
     {:href "https://github.com/walkable-server/minimalistic-cljs"}
     "Minimal Cljs Project"]
    #_[:a.py-2.px-4.md:py-1.md:px-2.rounded-sm.text-gray-500.border.border-gray-500
     {:href "/walkable/1.3.0/tutorial.html"}
     "Tutorial"]
    [:a.py-2.px-4.md:py-1.md:px-2.rounded-sm.text-gray-500.border.border-gray-500
     {:href "/walkable/1.3.0/introduction.html"}
     "Read Documentation"]
    [:a.py-2.px-4.md:py-1.md:px-2.rounded-sm.text-gray-500.border.border-gray-500
     {:href "/walkable/1.3.0/faq.html"}
     "FAQs"]]
   [:p.my-4.text-center "(more coming soon)"]])

(defn stages-of-development []
  [:div.flex.justify-center.py-12.bg-blue-50 {}
   [:div.px-2.py-4 {:class "md:w-3/5"}
    [:h2.text-center.text-lg.text-gray-800.mb-8 {}
     "Walkable works for any stage of development"]
    [:div.flex.flex-col.md:flex-row {}
     [:div.grid.grid-rows-2.grid-cols-sd {}
      (svg :rocket :svg.my-2.h-10.mx-auto)
      [:div {}
       [:div.text-sm.text-blue-500.font-semibold "Green field projects"]
       [:div.text-sm {}
        "suitable for rapid application development where constant change is expected."]]
      (svg :viking :svg.my-2.h-10.mx-auto)
      [:div {}
       [:div.text-sm.text-blue-500.font-semibold "Legacy apps"]
       [:div.text-sm {} "making more out of existing database. No change, no risk."]]
      (svg :centaur :svg.my-2.h-10.mx-auto)
      [:div {}
       [:div.text-sm.text-blue-500.font-semibold "Mix of old and new code"]
       [:div.text-sm {} "it's totally fine to gradually introduce new code as the system evolves."]]]
     [:div.flex.flex-col.justify-center {}
      (svg :construction :svg.m-4.h-40)
      [:div.text-sm.text-center {} "Build reliable apps, fast!"]]]]])

(defn checkbox-col [col]
  (if (boolean? col)
    [:td.py-2.text-center.border.text-green-600 {}
     (when col (svg :check :svg.h-4.mx-auto.fill-current))]
    [:td.px-4.py-2.border {} col]))

(defn simple-col [col]
  [:td.px-4.py-2.border {} col])

(defn build-table
  [f table headers content]
  (let [n-cols (count headers)]
    (into table
      [[:thead {}
        (into [:tr {}]
          (map #(do [:th.px-4.py-2.border {} %]) headers))]
       (into [:tbody {}]
         (map (fn [cols odd?]
                (into [:tr (if odd? {:class "bg-gray-100"} {})]
                  (mapv f cols)))
           (partition n-cols content)
           (cycle [true false])))])))

(defn range-of-applications []
  [:div.py-12.bg-white {}
   [:div.container.mx-auto.px-8 {}
    [:h2.mb-8.text-xl.leading-9.text-center.text-gray-700 {}
     "Walkable supports a wide range of environments"]
    [:div.grid.grid-cols-1.sm:grid-cols-2 {}
     (let [star (svg:star :svg.inline-block.h-4.-ml-5.mt-1.absolute.fill-current.text-yellow-500)]
       [:ul.px-4.pb-4.justify-center.leading-normal {}
        svg:defs:star
        [:li.pt-2 star "traditional web aka multi-page applications"]
        [:li.pt-2 star "backend for Clojurescript single-page applications"]
        [:li.pt-2 star "API only (think REST, Graphql or EQL)"]
        [:li.pt-2 star "React native app with SQLite storage"]])
     (build-table checkbox-col
       [:table.table-auto {}]
       ["Database" "Officially supported?"]
       ["Mysql" true
        "SQLite" true
        "SQL server" false
        "Oracle" false
        "Postgresql" true])]]])

(defn at-a-glance-section []
  [:div.py-12.bg-white {}
   [:h1.mb-8.text-center.text-xl.text-gray-700 {}
    "Walkable at a glance"]
   [:div.flex.flex-col.md:flex-row.justify-center {}
    ;; 56 = (6+44+6)
    ;; 44 = (24+10+10) = (6+32+6)
    [:div.flex.h-56.py-6.justify-center.md:justify-start {}
     [:div.relative.w-48.md:mr-10 {}
      svg:defs:single-layer
      (svg:single-layer :svg.absolute.top-0.mt-0.left-0.md:mx-4.h-24.fill-current.text-blue-500.z-20)
      (svg:single-layer :svg.absolute.top-0.mt-10.left-0.md:mx-4.h-24.fill-current.text-gray-700.z-10)
      (svg:single-layer :svg.absolute.top-0.mt-20.left-0.md:mx-4.h-24.fill-current.text-green-500.z-0)]]

    [:div.md:h-56.md:py-6.grid.grid-flow-row.grid-cols-1.md:grid-cols-2.grid-rows-3 {}
     (def-hover-tabset-labels
       [:label.mx-auto.w-56.p-1.cursor-pointer.rounded.hover:bg-blue-100.order-1
        {}
        [:h2.text-sm.font-semibold.text-blue-500 {}
         "Query language layer"]
        [:p.text-xs "Describe data requirements with expressive Clojure-native language"]]
       [:label.mx-auto.w-56.p-1.cursor-pointer.rounded.hover:bg-blue-100.order-2
        {}
        [:h2.text-sm.font-semibold.text-gray-700 {}
         "Security layer"]
        [:p.text-xs "Automatically inject appropriate condition clauses"]]
       [:label.mx-auto.w-56.p-1.cursor-pointer.rounded.hover:bg-blue-100.order-3
        {}
        [:h2.text-sm.font-semibold.text-green-500 {}
         "Adaptation layer"]
        [:p.text-xs "Flexible way to produce SQL strings that fits your needs"]])
     
     (def-hover-tabset-contents
       [:div.w-full.row-span-3.order-first.md:order-none {}
        (svg :clojure :svg.mx-auto.m-6.h-32.w-40.px-6.fill-current.text-blue-500)]
       [:div.w-full.row-span-3.order-first.md:order-none.hidden {}
        (svg :security :svg.mx-auto.m-6.h-32.w-40.px-6.fill-current.text-gray-700)]
       [:div.w-full.row-span-3.order-first.md:order-none.hidden {}
        (svg :adaptation :svg.mx-auto.m-6.h-32.w-40.px-6.fill-current.text-green-500)])]]])

(def demo-code-query
  (clojure.string/trim "
[{(:articles/list
 {:filter [:and [:= false :article/hidden]
                {:article/author [:= :user/username \"lucy\"]}]})
  [:article/title
   :article/created-date
   {:article/author [:user/id :user/username :user/karma]}]}]"))

(def demo-code-result
  (clojure.string/trim "
{:articles/list
 [{:article/title \"Welcome\"
   :article/created-date \"2018-11-10\"
   :article/author {:user/id 20
                    :user/username \"lucy\"
                    :user/karma 42}}]}"))

(def code-style
  :code.language-clojure.hljs.rounded)

(defn demo-section []
  [:div.px-2.lg:px-16.py-12.bg-blue-300 {}
   [:pre.container.grid.grid-cols-1.lg:grid-cols-3.gap-y-4.lg:justify-items-center.highlightjs.highlight.lg {}
    [code-style {:class "lg:col-span-3"
                 :data-lang "clojure"}
     demo-code-query]
    [:div.lg:col-span-2.overflow-x-scroll {}
     (build-table simple-col
       [:table.table-auto.bg-white {}]
       ["id" "title" "author_id" "hidden" "created_date"]
       [1 "Hello world" 10 false "2018-10-11"
        2 "Welcome" 20 false "2018-11-10"
        3 "Unfinished" 20 true "2018-09-20"])]
    [:div {}
     (build-table simple-col
       [:table.table-auto.bg-white {}]
       ["id" "username" "karma"]
       [10 "mark" 21
        20 "lucy" 42])]
    [code-style {:class "lg:col-span-3"
                 :data-lang "clojure"}
     demo-code-result]]])

(def home-page
  [:html.text-gray-900.antialiased.text-base
   {:lang "en"}
   [:head {}
    [:meta {:charset "UTF-8"}]
    [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
    [:link {:rel "stylesheet",
            :href "home.css",
            :type "text/css",
            :media "screen"}]
    [:link {:rel "stylesheet",
            :href "/_/css/tomorrow-night-eighties.css",
            :type "text/css",
            :media "screen"}]
    [:style (hover-tabs-css)]]
   [:body.min-h-screen.text-gray-700 {}
    [:div.w-100.mx-auto {}
     (hero-section)
     (demo-section)
     (at-a-glance-section)
     (stages-of-development)
     (range-of-applications)
     (saves-time-section)
     (convince-section)]
    [:script {:src "_/js/vendor/highlight.js"}]]])

(defn -main [& _args]
  (spit "public/index.html"
    (str "<!doctype html>"
      (html/html home-page))))
