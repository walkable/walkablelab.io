(defproject walkable-doc "0.1.0-SNAPSHOT"
  :description "Homepage generator"
  :url "https://walkable.gitlab.io"
  :license {:name "Eclipse Public License"
            :url "https://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [hiccup "1.0.5"]]
  :main ^:skip-aot clj.home)
